import Vue from "vue";
import Vuex from "vuex";
import { LastFmClient } from "./lastfm";
import { get, set, del } from "idb-keyval";
import {
  LASTFM_API_KEY,
  LASTFM_SECRET,
  SESSION_KEY_KEY,
  USERNAME_KEY
} from "./constants";

const lfmCl = new LastFmClient(LASTFM_API_KEY, LASTFM_SECRET);

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    session: "",
    username: ""
  },
  getters: {
    haveSession({ session }) {
      return Boolean(session);
    }
  },
  mutations: {
    login(state, payload: { session: string; username: string }) {
      state.session = payload.session;
      state.username = payload.username;
    },
    logout(state) {
      state.session = "";
      state.username = "";
    }
  },
  actions: {
    async loadFromIndexedDB({ commit }) {
      const [session, username] = await Promise.all([
        get(SESSION_KEY_KEY),
        get(USERNAME_KEY)
      ]);
      if (session && username) commit("login", { session, username });
    },
    async loginWithToken({ commit }, token: string) {
      const response = await lfmCl.authGetSession(token);
      commit("login", {
        session: response.session.key,
        username: response.session.name
      });
    }
  },
  plugins: [
    store => {
      store.subscribe(async ({ type }, state) => {
        switch (type) {
          case "login": {
            await Promise.all([
              set(SESSION_KEY_KEY, state.session),
              set(USERNAME_KEY, state.username)
            ]);
            break;
          }
          case "logout":
            await Promise.all([del(SESSION_KEY_KEY), del(USERNAME_KEY)]);
            break;
        }
      });
    }
  ],
  modules: {}
});

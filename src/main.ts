import Vue from "vue";
import App from "./App.vue";
import { ModalPlugin } from "bootstrap-vue";
import "./custom.scss";
import "bootstrap-vue/dist/bootstrap-vue.css";
import { library } from "@fortawesome/fontawesome-svg-core";
import { faLastfm } from "@fortawesome/free-brands-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
// import router from "./router";
import store from "./store";
import { get } from "idb-keyval";

import { SESSION_KEY_KEY, USERNAME_KEY } from "./constants";

library.add(faLastfm);

Vue.config.productionTip = false;

Vue.use(ModalPlugin);

Vue.component("font-awesome-icon", FontAwesomeIcon);

(async () => {
  const [session, username] = await Promise.all([
    get(SESSION_KEY_KEY),
    get(USERNAME_KEY)
  ]);
  window.vm = new Vue({
    el: "#app",
    // router,
    store,
    render: h => h(App),
    created() {
      if (session && username)
        this.$store.commit("login", { session, username });
    }
  });
})();

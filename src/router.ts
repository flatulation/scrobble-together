import Vue from "vue";
import VueRouter from "vue-router";
import Home from "./views/Home.vue";
import NotFound from "./views/NotFound.vue";

Vue.use(VueRouter);

const router = new VueRouter({
  routes: [
    {
      path: "/",
      component: Home
    },
    {
      path: "/user",
      component: () => import("./views/User.vue")
    },
    {
      path: "*",
      redirect(to) {
        return {
          path: "/",
          replace: true
        };
      }
    }
  ]
});

export default router;

export const SESSION_KEY_KEY = "lastfm_session";
export const USERNAME_KEY = "lastfm_username";
export const LASTFM_API_KEY = process.env.VUE_APP_LASTFM_API_KEY;
export const LASTFM_SECRET = process.env.VUE_APP_LASTFM_SECRET;

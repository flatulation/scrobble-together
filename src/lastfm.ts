import SparkMD5 from "spark-md5";

export interface AuthGetSessionResponse {
  session: {
    name: string;
    key: string;
  };
}

class LastFmError extends Error {
  public code: number | string;

  constructor(code: number | string, message: string) {
    super(message);
    this.name = "LastFmError";
    this.code = code;
  }
}

export class LastFmClient {
  LASTFM_API_URL = "https://ws.audioscrobbler.com/2.0/";

  public apiKey: string;
  private secret: string;

  constructor(apiKey: string, secret: string) {
    this.apiKey = apiKey;
    this.secret = secret;
  }

  private constructUrl(method: string, params: object) {
    const url = new URL(this.LASTFM_API_URL);

    url.searchParams.set("api_key", this.apiKey);
    url.searchParams.set("method", method);
    Object.entries(params).forEach(([k, v]) => url.searchParams.set(k, v));

    const allParams = Array.from(url.searchParams.entries());
    allParams.sort((a, b) => {
      const k1 = a[0];
      const k2 = b[0];
      if (k1 > k2) return 1;
      if (k2 > k1) return -1;
      return 0;
    });
    const signature =
      allParams.reduce((acc, [k, v]) => acc + k + v, "") + this.secret;
    const sigHash = SparkMD5.hash(signature);
    url.searchParams.set("api_sig", sigHash);
    url.searchParams.set("format", "json");

    return url;
  }

  private async GET(method: string, params: object): Promise<any> {
    const url = this.constructUrl(method, params);
    const resp = await fetch(url.href);
    const body = await resp.json();
    if (body.error) {
      throw new LastFmError(body.error, body.message);
    }
    return body;
  }

  authGetSession(token: string): Promise<AuthGetSessionResponse> {
    return this.GET("auth.getSession", { token });
  }
}

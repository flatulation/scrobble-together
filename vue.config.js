// const WorkerPlugin = require("worker-plugin");

module.exports = {
  lintOnSave: false,
  configureWebpack: {
    devtool: "source-map",
    resolve: {
      alias: {
        // Alias for using source of BootstrapVue
        "bootstrap-vue$": "bootstrap-vue/src/"
      }
    }
  },
  chainWebpack: config => {
    config.module
      .rule("vue")
      .use("vue-loader")
      .tap(args => {
        args.compilerOptions.whitespace = "preserve";
      });
    // config.module
    //   .rule("worker")
    //   .test(/\.worker\.js$/)
    //   .use()
    //   .loader("worker-loader")
    //   .end();
  },
  pluginOptions: {
    webpackBundleAnalyzer: {
      openAnalyzer: false
    }
  }
};
